const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");
const $itemText = $(".item-text");

const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];
/////////////////
$(document).ready(function(){
  todos.forEach(obj => printTask(obj));
  $add.on("click",function(e){
   if($input.val() != "")
   {
      let taskObj = {};
      taskObj.text = $input.val();
      taskObj.done = false;
      todos.push(taskObj);
      printTask(taskObj);
   }
 });
function printTask(obj){
   let textClass = 'item-text';
   if (obj.done === true) textClass = 'item-text done';
   let todo = $(`
      <li class="item">
         <span class="${textClass}">${obj.text}</span>
         <button class="item-remove">Remove</button>
      </li>`);

   todo.on("click",function(){
      $(this).children('span').toggleClass('done');
      obj.done = !obj.done;
   });

   todo.children('button').on("click", function() {
      $(this).parent().remove();
      removeFromList(obj, todos);
   });
   $list.append(todo);
}
function removeFromList(obj,arr){

   console.log(arr);
   const index = arr.indexOf(obj);
   if (index > -1) {
      arr.splice(index, 1);
   }
   console.log(arr); 
}
});

