// Your code goes here
function maxElement(arr){
   return Math.max.apply(null, arr);
}
function copyArray(arr){
   let newArr = [];
   for (let elem of arr){
      newArr.push(elem);
   }
   return newArr;
}
function addUniqueId(obj){
   let id = Symbol('id');
   obj[id] = 113443;
   return obj;
}
// function regroupObject(obj) {
// };
function finduniqueElements(arr){
   const unique = (value, index, self) => {
      return self.indexOf(value) === index;
   }
   const uniqueArr = arr.filter(unique);
   return uniqueArr;
}
function hideNumber(number){
   return number.slice(-4).padStart(10,'*');
}
// function add(a, b){

// }

