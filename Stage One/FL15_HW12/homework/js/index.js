function visitLink(path) {
   let page1Num = localStorage.getItem('page1');
   let page2Num = localStorage.getItem('page2');
   let page3Num = localStorage.getItem('page3');
   switch (path) {
      case 'Page1':
         if(!localStorage.getItem('page1')){
            localStorage.setItem('page1', 0);
         }
         page1Num++;
         localStorage.setItem('page1', page1Num);
         break;
      case 'Page2':
         if(!localStorage.getItem('page2')){
            localStorage.setItem('page2', 0);
         }
         page2Num++;
         localStorage.setItem('page2', page2Num);
         break;
      case 'Page3':
         if(!localStorage.getItem('page3')){
            localStorage.setItem('page3', 0);
         }
         page3Num++;
         localStorage.setItem('page3', page3Num);
         break;
      default:
         break;
   }
}

function viewResults() {
   if(!localStorage.getItem('page1')){
      localStorage.setItem('page1', 0);
   }
   if(!localStorage.getItem('page2')){
      localStorage.setItem('page2', 0);
   }
   if(!localStorage.getItem('page3')){
      localStorage.setItem('page3', 0);
   }
   if(!document.getElementById('count-monitor')){
      let resultDiv = document.getElementById('content');
      resultDiv.innerHTML += `
      <ul id ='count-monitor'>
      </ul>`;
   }
   let resultList = document.getElementById('count-monitor');
   resultList.innerHTML = `
   <li>You visited Page1 ${localStorage.getItem('page1')} time(s)</li>
   <li>You visited Page2 ${localStorage.getItem('page2')} time(s)</li>
   <li>You visited Page3 ${localStorage.getItem('page3')} time(s)</li>`;

	
   localStorage.clear();
}
