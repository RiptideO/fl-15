'use strict';
window.onload = function(){
   let regExp= new RegExp(/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/);
   let eventName = prompt('Event Name', 'meeting');
   let form = document.getElementById('form');
   form.style.display = 'block';
   let confirmButton = document.getElementById('confirm-btn');
   confirmButton.onclick = function(){
      let name = document.getElementById('name');
      let time = document.getElementById('time');
      let place = document.getElementById('place');

      if(name.value ==='' || time.value ==='' ||place.value ===''){
         alert('Input all data');
      } else if(!regExp.test(time.value)){
         alert('Enter time in format hh:mm');
      } else{
         console.log(`${name.value} has a ${eventName} today at ${time.value} somewhere in ${place.value}`);
      }
   }
   let converterButton = document.getElementById('converter-btn');
   converterButton.onclick = function(){
      let fixedNumbers = 2;
      let euroCourse = 32.93;
      let dollarCourse = 27.76;
      let userEuro = Number(prompt('Input amout of euro'));
      while(userEuro < 0 || isNaN(userEuro)){
         alert('Amount of money should be positive number');
         userDollars = Number(prompt('Input amout of euro'));
      }
      let euroUah = userEuro * euroCourse;
      euroUah = euroUah.toFixed(fixedNumbers);

      let userDollars = Number(prompt('Input amout of dollars'));
      while(userDollars < 0 || isNaN(userDollars)){
         alert('Amount of money should be positive number');
         userDollars = Number(prompt('Input amout of dollars'));
      }
      let dollarsUah = userDollars * dollarCourse;
      dollarsUah = dollarsUah.toFixed(fixedNumbers);

      alert(`${userEuro} euros are equal ${euroUah} hrns,\n${userDollars} dollars are equal ${dollarsUah} hrns`);
   }
}


