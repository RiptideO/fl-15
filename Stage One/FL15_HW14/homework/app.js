let appRoot = document.getElementById('app-root');
appRoot.innerHTML = `      
   <h1>Countries Search</h1>
   <div class="choose-box"> 
      <p>Please choose type of search: </p>
      <div class="input-wrap">
         <input type="radio" class="radio-btn" name="search-type" id="search-reg" > 
         <label for="search-reg">By Region</label><br>
         <input type="radio" class="radio-btn" name="search-type" id="search-lang">
         <label for="search-lang">By Language</label>
      </div>  
   </div>   
   <div class="choose-box">   
      <p>Please choose search quary:</p>
      <select id="select">
         <option selected disabled>Select Value</option>
      </select>
   </div>
   <table id='table' class="table-sortable">
      <thead id="table-head">
      </thead>
      <tbody id="table-body">
      </tbody>
   </table>
   
`;
let tableBody = document.getElementById('table-body');
let tablehead = document.getElementById('table-head');
let selectQuery = document.getElementById('select');
let searchReg = document.getElementById('search-reg');

let searchLang = document.getElementById('search-lang');

let regList = externalService.getRegionsList();
let langList = externalService.getLanguagesList();


searchReg.addEventListener('click', function(){
   if(searchReg.checked){
      selectQuery.innerHTML = '<option selected disabled>Select Region</option>';
      regList.forEach(el => {
         selectQuery.innerHTML += `<option value= "${el}">${el}</option>`;
      });
      
          
   }
});

searchLang.addEventListener('click', function(){
   if(searchLang.checked){
      selectQuery.innerHTML = '<option selected disabled>Select Language</option>';
      langList.forEach(el => {
         selectQuery.innerHTML += `<option value= "${el}">${el}</option>`;
      });
   }
});

selectQuery.addEventListener('change', function(){
   
   tablehead.innerHTML= `   
   <tr>
      <th>Country Name</th>
      <th>Capital</th>
      <th>World Region</th>
      <th>Languages</th>
      <th>Area</th>
      <th>Flag</th>
   </tr>`;
   tableBody.innerHTML = ``;
   
if (searchReg.checked){
   let choosenCountries = externalService.getCountryListByRegion(selectQuery.value);
   
   choosenCountries.forEach(el => {
      tableBody.innerHTML += `   
   <tr>
      <td>${el.name}</td>
      <td>${el.capital}</td>
      <td>${el.region}</td>
      <td>${Object.values(el.languages)}</td>
      <td>${el.area}</td>
      <td><img src="${el.flagURL}"></td>
   </tr>`;
   });
} else if (searchLang.checked){
   let choosenCountries = externalService.getCountryListByLanguage(selectQuery.value);
  
   choosenCountries.forEach(el => {
      tableBody.innerHTML += `   
   <tr>
      <td>${el.name}</td>
      <td>${el.capital}</td>
      <td>${el.region}</td>
      <td>${Object.values(el.languages)}</td>
      <td>${el.area}</td>
      <td><img src="${el.flagURL}"></td>
   </tr>`;
   });
}

document.querySelectorAll('.table-sortable th').forEach( th => {
   th.addEventListener('click', () => {
      let tableEl = th.parentElement.parentElement.parentElement;
      let headerIndex = Array.prototype.indexOf.call(th.parentElement.children, th);
      let currentIsAscending = th.classList.contains('th-sort-asc');

      sortTableByCol(tableEl, headerIndex, !currentIsAscending);
   }
   
) 
});
});

function sortTableToggle(){
   let tableEl = th.parentElement.parentElement.parentElement;
   let headerIndex = Array.prototype.indexOf.call(th.parentElement.children, th);
   let currentIsAscending = th.classList.contains('th-sort-asc');

   sortTableByCol(tableEl, headerIndex, !currentIsAscending);
}
function sortTableByCol(table, col, asc = true) { 
   let dirMod = asc ? 1 : -1;
   let tBody = table.tBodies[0];
   let rows = Array.from(tBody.querySelectorAll('tr'));



   //sort
   let sortedRows = rows.sort((a,b) => {

         let aText = a.querySelector(`td:nth-child(${col+1})`).textContent;
         let bText = b.querySelector(`td:nth-child(${col+1})`).textContent;
         if(!isNaN(aText)&&!isNaN(aText)){
            aText = parseInt(aText);
            bText = parseInt(bText);
         }
         return aText > bText ? 1*dirMod : -1 * dirMod;
   });
   //deleting old rows
   while(tBody.firstChild){
      tBody.removeChild(tBody.firstChild);
   }
   //adding new rows
   tBody.append(...sortedRows);

   table.querySelectorAll('th').forEach(th => th.classList.remove('th-sort-asc', 'th-sort-desc'));
   table.querySelector(`th:nth-child(${col + 1})`).classList.toggle('th-sort-asc', asc);
   table.querySelector(`th:nth-child(${col + 1})`).classList.toggle('th-sort-desc', !asc);

}



