'use strict';
function isEquals(a,b){
   return a===b;
}
function isBigger(a,b){
   return a>b;
}
function storeNames(){
   let nameArr = [];
   for (let i = 0; i < arguments.length; i++){
      nameArr.push(arguments[i]);
   }
   return nameArr;
}
function getDifference(a, b){
   if(b>a){
      return b-a;
   } else{
      return a-b;
   }
}
function negativeCount(arr=[]){
   let negNumbers = arr.filter(el => el < 0);
   return negNumbers.length;
}
function letterCount(word, letter){
   letter= letter.toLowerCase();
   word = word.toLowerCase();
   word = word.split('');
   let foundLetters = 0;
   foundLetters = word.filter(el => el === letter);
   return foundLetters.length;
}
function countPoints(arr=[]){
   let finalScore = arr.reduce((acc,match) => {
      if(Number(match.split(':')[0]) > Number(match.split(':')[1])){
         acc += 3;
      } else if(Number(match.split(':')[0]) === Number(match.split(':')[1])){
         acc += 1;
      }
      return acc;
   },0);
   return finalScore;
}
