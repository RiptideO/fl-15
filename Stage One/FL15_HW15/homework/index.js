/* START TASK 1: Your code goes here */
let table = document.getElementById('table');
let firstColCells = table.querySelectorAll('tr :first-child');// first col cells
firstColCells = Array.from(firstColCells);// from nodeList into array
let allCells = table.querySelectorAll('td');
allCells = Array.from(allCells);// all cells
let diffCells = allCells.filter(x => !firstColCells.includes(x)); // all cells except first col cells 



function paintBlue(cells){
   let specCell = document.getElementById('special-cell');
   cells.forEach(cell => { 
      if (cell !== specCell){
         cell.addEventListener('click', () => {
            let nextCell = cell.nextElementSibling;
            if(!cell.classList.contains('bg-yellow')){ // non need to add bg-blue class if bg-yellow exists on this cell
               cell.classList.add('bg-blue');
               cell.classList.remove('bg-green'); // clearing cell class from bg-green if it exists.
            }
            while(nextCell){ // do the same but for other cells in  this row
               if(!nextCell.classList.contains('bg-yellow')){
                  nextCell.classList.add('bg-blue');
                  nextCell.classList.remove('bg-green');
               }
               nextCell = nextCell.nextElementSibling;
            }
         });
      }

   });
}
function paintYellow(cells){
   let specCell = document.getElementById('special-cell');
   cells.forEach(cell => { 
      if(cell !==specCell){
         cell.addEventListener('click', () => {
            cell.classList.remove('bg-green', 'bg-blue');
            cell.classList.add('bg-yellow');
         });
      }

   });
}
function paintGreen(cells){
   let specCell = document.getElementById('special-cell');
   specCell.addEventListener('click', () => {
      cells.forEach(cell => {
         if(!cell.classList.contains('bg-yellow') && !cell.classList.contains('bg-blue') ){
            cell.classList.add('bg-green');
         }
      })
   });
}
paintBlue(firstColCells);
paintYellow(diffCells);
paintGreen(allCells);
/* END TASK 1 */

/* START TASK 2: Your code goes here */
let inputNumField = document.getElementById('number-field');
let sendBtn = document.getElementById('send-btn');
inputNumField.addEventListener('input', checkField);
let notification = document.getElementById('notification');

sendBtn.addEventListener('click', () => {
   notification.innerText = 'Data was successfully sent';
   notification.classList.add('success');
   notification.classList.remove('danger');
})

function checkField(){
   let field = this;
   if(isValidInput(field)){
      sendBtn.removeAttribute('disabled');
      field.style.borderColor ='black';
      notification.classList.remove('danger');
      notification.classList.remove('success');
      notification.innerText = '';
   } else{
      sendBtn.setAttribute('disabled', 'disabled');
      notification.innerText = 'Type number does not follow format +380*********';
      notification.classList.add('danger');
      notification.classList.remove('success');
      field.style.borderColor ='#ff8080';
   }
}
function isValidInput(field) {
   field = field.value;
   let regexp = /^\+380([0-9]{9})$/i;

   return regexp.test(field);

}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
let court = document.getElementById('court');
let ball = document.getElementById('ball');
let bucketA = document.getElementById('bucketA');
let bucketB = document.getElementById('bucketB');
let goalBlock = document.getElementById('goal');
let scoreA = document.getElementById('score-A');
scoreA.innerText = 0;
let scoreB = document.getElementById('score-B');
let delay = 3000;
let divider = 2;
scoreB.innerText = 0;
bucketA.addEventListener('goal', () => {
   goalBlock.innerText = 'Team B score!';
   goalBlock.style.color = 'red';
   scoreB.innerText++;
   setTimeout(() => {
      goalBlock.innerText = '';
   }, delay);

});

bucketB.addEventListener('goal', () => {
   goalBlock.innerText = 'Team A score!';
   goalBlock.style.color = 'blue';
   scoreA.innerText++;
   setTimeout(() => {
      goalBlock.innerText = '';
   }, delay);

});
function moveBall(e){
   
   let posX = e.clientX - court.offsetLeft;
   let posY = e.clientY - court.offsetTop;
   ball.style.left = posX + 'px';
   ball.style.top = posY + 'px';
   if(ball.offsetLeft < ball.offsetWidth/divider){
      ball.style.left = ball.offsetWidth/divider + 'px';
   } else if(ball.offsetLeft > court.offsetWidth - ball.offsetWidth/divider){
      ball.style.left = court.offsetWidth - ball.offsetWidth/divider + 'px';
   }

   if(ball.offsetTop < ball.offsetWidth){
      ball.style.top = ball.offsetWidth/divider + 'px';
   } else if(ball.offsetTop > court.offsetHeight - ball.offsetHeight/divider){
      ball.style.top= court.offsetHeight - ball.offsetHeight/divider + 'px';
   }

   if(bucketA.offsetLeft <= posX && posX <=bucketA.offsetLeft + bucketA.offsetWidth && (
      bucketA.offsetTop <= posY && posY <= bucketA.offsetTop + bucketA.offsetHeight)){
         bucketA.dispatchEvent(goalEvent);
   } else if(bucketB.offsetLeft <= posX && posX <=bucketB.offsetLeft + bucketB.offsetWidth && (
      bucketB.offsetTop <= posY && posY <=bucketB.offsetTop + bucketB.offsetHeight)){
         bucketB.dispatchEvent(goalEvent);
   }

}


let goalEvent = new CustomEvent('goal', {
 });

court.addEventListener('click', moveBall);


/* END TASK 3 */
