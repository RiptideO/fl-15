function getAge(date){
   let currDate = new Date();// current date 
   let dateDiff =(currDate - date) / 1000;
   dateDiff /= 60 * 60 * 24;
   return Math.floor(dateDiff/365.25);
}

function getWeekDay(date){
   let weekdayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
  'Saturday'];
   date = new Date(date); // needed if we use date.now() as an argument
   return weekdayNames[date.getDay()];
}

function getAmountDaysToNewYear(){
   let currDate = new Date();
   let newYearDate = new Date(currDate.getFullYear() + 1, 0, 1);// curr year + 1 = next year
   let toNewYearDays = (newYearDate - currDate) / 1000;
   toNewYearDays /= 60 * 60 * 24; // in days to New Year
   return Math.floor(toNewYearDays);
}

function getProgrammersDay(year){

   let monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

   let startDate = new Date(year, 0, 0); // 1-st Jan
   let programmerday = 256;
   programmerday *= 60 * 60 * 24 * 1000;// transforming to ms
   let dateSum = startDate.getTime() + programmerday;
   dateSum = new Date(dateSum);

   console.log(`${dateSum.getDate()} ${monthNames[dateSum.getMonth()]}, 
   ${dateSum.getFullYear()} (${getWeekDay(dateSum)})`);
   
}

function howFarIs(day){
   let weekdayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
   'Saturday'];
   let currDate = new Date();
   let daysLeft;
   let dayNum;
   for(let i = 0; i < weekdayNames.length; i++){
      if(weekdayNames[i].toLowerCase() === day.toString().toLowerCase() ){
         dayNum = i;
      }
   }
   if(!dayNum) {
 return `There is no such day as ${day}`; 
}

   if(currDate.getDay() > dayNum){
      daysLeft = 7 - (currDate.getDay() - dayNum);
   
   } else {
 daysLeft = dayNum - currDate.getDay(); 
}

   
   if(daysLeft === 0 ){
      return `Hey, today is ${weekdayNames[dayNum]} =)`;
   } else {
 return `It's ${daysLeft} day(s) left till ${weekdayNames[dayNum]} =)`; 
}
   
}

function isValidIdentifier(str) {
   let regexp = /^[a-z_$][a-z0-9_$]*$/i
   return regexp.test(str);
}

function capitalize(str){
   str = str.replace(/\b\w/g, function(letter) {
   return letter.toUpperCase();
});
  return str;
}

function isValidAudioFile(str){
   let regexp = /^([a-zA-Z])+(.mp3|.flac|.alac|.aac)$/
   return regexp.test(str);
}

function getHexadecimalColors(str){
   let regexp = /#([0-9a-f]{3}|[0-9a-f]{6})$/i
   str = str.replaceAll(';',':');
   str = str.split(':');

   let color = str.filter(color => regexp.exec(color));
   return color;
   
}

function isValidPassword(str){
   return(
      /\d+/.test(str) &&
      /[A-Z]+/.test(str) &&
      /[a-z]+/.test(str) &&
      str.length >=8
   );
}

function addThousandsSeparators(val){
   return val.toString().replace(/(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(str){
   let regexp = /^https?:\/\/(.*)$/i;
   str = str.split(' ');
   let links = str.filter(link => regexp.exec(link));
   return links;
}