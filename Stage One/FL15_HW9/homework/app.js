function reverseNumber(num) {
   num = num +'';
   let res='';

   for(let i = num.length - 1; i>=0; i--){

      if(num[i] ==='-'){
         res *= -1;
         break;
      }

      res += num[i];
   }
   console.log(res);
}

function forEach(arr, func) {
   for(let el of arr){
      func(el);
   }
}

function map(arr, func) {
   let res = [];
   forEach(arr, function(el){
      res.push(func(el));
   });
   console.log(res);
   return res;
}

function filter(arr, func) {
   let res = [];
   forEach(arr, function(el){
      if(func(el)){
         res.push(el);
      }
   });
   console.log(res);
   return res;

}

function getAdultAppleLovers(data) {
   let res = [];
   for (let el of data) {
      if(el.favoriteFruit ==='apple' && el.age > 18){
         res.push(el.name);
      }
   }
   console.log(res);
}

function getKeys(obj) {
   let keys = [];
   if(obj !== null){
      for (let key in obj) {
      keys.push(key);
   }
}
   
   console.log(keys);
}

function getValues(obj) {
   let values = [];
   for (let key in obj) {
      let value = obj[key];
      values.push(value);
   }
   console.log(values);
}

function showFormattedDate(dateObj) {
   let options = {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
   };
   console.log(`It is ${dateObj.toLocaleString('en-GB', options)}`);
}

 

