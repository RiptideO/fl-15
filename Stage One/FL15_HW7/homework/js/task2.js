'use strict';
let baseEndRange = 8;
const attempts = 3;
let baseWin = 25;
let winModifier = 2;
let lvl= 2; 
let total = 0;
let startGame = confirm('Do you want to play a game?');
if(startGame){
   createGame(baseEndRange, attempts, baseWin, winModifier, lvl, total);
} else{
   alert('You did not become a billionaire, but can.');
}
function createGame(endRange ,attemptsAmount, win, winMod , lvl, total){
   let rangeModifier = 4;
   let currWin = win * Math.pow(winMod,attemptsAmount-1) * lvl;
   let winNum = Math.round(Math.random()*endRange);
   let ans = userAnswer(endRange, attemptsAmount, total, currWin);
   for(let i = 1; i <= attemptsAmount; i++){
      currWin = win * Math.pow(winMod,attemptsAmount-i)*lvl;
      if(ans === winNum){  
         total += currWin;
         let continueGame = confirm('Congratulation, you won!\nYour prize is: ' + total + 
         '$.\nWin number was: ' + winNum +
         '\nYour answer was: '+ ans +
         '\nDo you want to continue?');
         if(continueGame){
            nextLevel(endRange ,attemptsAmount, win, winMod , lvl, total,rangeModifier);
         } else{
            alert('Thank you for your participation. Your prize is: ' + total + '$');
         }
         break;
      } else if(i === attemptsAmount){
         alert('Thank you for your participation. Your prize is: ' + total + '$'+
         '\nWin number was: ' + winNum+
         '\nYour answer was: '+ ans);
         break;
      } else{
         alert('try again');
         let j = 1;
         currWin = win * Math.pow(winMod,attemptsAmount-i-j)*lvl;
         ans = userAnswer(endRange, attemptsAmount-i, total, currWin);
      }
   }
}


function userAnswer(endRange ,attemptsleft, total, possibleWin){
   let ans = Number(prompt('Choose a roulette pocket number from 0 to '+ endRange +
   '\nAttempts left: '+ attemptsleft +
   '\nTotal prize: ' + total + '$'+
   '\nPossible prize on current attempt: '+ possibleWin +'$'));
   while(isNaN(ans)){
      alert('Invalid input data');
      ans = Number(prompt('Choose a roulette pocket number from 0 to '+ endRange +
   '\nAttempts left: '+ attemptsleft +
   '\nTotal prize: ' + total + '$'+
   '\nPossible prize on current attempt: '+ possibleWin +'$'));
   }
   return ans;
}
function nextLevel(endRange ,attemptsAmount, win, winMod , lvl, total, rangeModifier){
   lvl*=winMod;
   endRange+=rangeModifier;
   createGame(endRange ,attemptsAmount, win, winMod , lvl, total);
}