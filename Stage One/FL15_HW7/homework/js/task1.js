'use strict';
let minimumDeposit = 1000;
let minimumYears = 1;
let minimumPercent = 0;
let maximumPercent = 100;
const percentDivider = 100;
const toFixedNum = 2;
let deposit = Number(prompt('Deposit'));
while(deposit < minimumDeposit || isNaN(deposit)){
   alert('Invalid input data \nDeposit can`t be less than 1000');
   deposit = Number(prompt('Deposit'));
}


let years = Number(prompt('Amout of years'));
while(years < minimumYears || isNaN(years)){
   alert('Invalid input data \nAmout of years can`t be less than 1');
   years = Number(prompt('Amout of years'));
}

let percent = Number(prompt('Percentage'));
while(percent < minimumPercent|| percent > maximumPercent || isNaN(deposit)){
   alert('Invalid input data \nDeposit can`t be less than 0 or more than 100');
   percent = Number(prompt('Percentage'));
}
function yearDeposit(deposit, years, percent){
   let profit;
   let account = deposit;
   for (let i = 0; i < years; i++){
      account += account * percent / percentDivider;
   }
   profit = account - deposit;
   if (account % 1 !== 0){
      account = account.toFixed(toFixedNum);
      profit = profit.toFixed(toFixedNum);
   }
   
      alert(' Initial account: '+ deposit +
      '\n Number of years: '+ years +
      '\n Percentage of year: '+ percent +'% ' +
      '\n\n Total profit: '+ profit +
      '\n Total account: '+ account);

   
}
yearDeposit(deposit, years, percent);
