//Classes

class Deck{
   constructor(){
      const deck = [];
      for(let i = 1; i < 5; i++){
         for(let j = 1; j < 14; j++){
            const card = new Card(i,j);
            deck.push(card);
         }
      }
      this.deck = deck;
      this._count = deck.length;
   }
   get count(){
      return this._count;
   }
   shuffle(){
      for(let i = this._count-1; i > 0; i--){
         let j = Math.floor(Math.random() * (i + 1));
         [this.deck[i], this.deck[j]] = [this.deck[j], this.deck[i]];
      }
   }
   draw(){
      let lastCard = this.deck.pop();
      this._count--;
      return lastCard;
   }
}
class Card{
   constructor(suit, rank){
      this.suit = suit;
      this.rank = rank;
      this._isFaceCard = () => {
         return this.rank === 1 || this.rank > 10;
      }
   }
   get isFaceCard(){
      return this._isFaceCard;
   }
   toString(){
      const suitList = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
      const rankList = ['Jack', 'Queen', 'King'];
      let strRank = this.rank;
      let strSuit = suitList[this.suit-1];
      if(this.isFaceCard()){
         if(this.rank === 1){
            strRank = 'Ace';
         } else{
            strRank = rankList[this.rank%10-1];
         }
      }

      return strRank + ' of ' + strSuit;

   }
   static compare(cardOne, cardTwo){
      if(cardOne.rank > cardTwo.rank){
         return cardOne;
      } else if(cardOne.rank < cardTwo.rank){
         return cardTwo;
      } else {
 return false; 
} // if returns false then draw no one gets point
   }  
}
class Player{
   constructor(name){
      this.name = name;
      this.wins = 0;
      this.deck = new Deck();
   }
   static play(playerOne, playerTwo){
      playerOne.deck.shuffle();
      playerTwo.deck.shuffle();  
      let i = playerOne.deck.count;
      while(playerOne.deck.count !== 0){
         

         let playerOneCard = playerOne.deck.draw();
         let playerTwoCard = playerTwo.deck.draw();

         console.log('Round '+ (i - playerOne.deck.count)+ ':\n\n' + playerOneCard +' vs ' + playerTwoCard +'\n\n');

         if(playerOneCard === Card.compare(playerOneCard, playerTwoCard)){
            playerOne.wins++;
         } else if(playerTwoCard === Card.compare(playerOneCard, playerTwoCard)){
            playerTwo.wins++;
         }
         
      }
      if(playerOne.wins > playerTwo.wins){
         return playerOne.name + ' wins! ' + playerOne.wins + ' to ' +playerTwo.wins;
      } else if(playerOne.wins < playerTwo.wins){
         return playerTwo.name + ' wins! ' + playerTwo.wins + ' to ' +playerOne.wins;
      } else {
 return 'Draw! ' + playerOne.name +' and '+ playerTwo.name + ' have same score !'; 
}

   }
}
let playerOne = new Player('Alex');
let playerTwo = new Player('Max');
console.log(Player.play(playerOne, playerTwo));

